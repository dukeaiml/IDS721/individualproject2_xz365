# Stage 1: Build Environment
FROM debian:buster-slim as builder

# Install necessary packages for building Rust application
RUN apt-get update \
    && apt-get install -y curl build-essential \
    && rm -rf /var/lib/apt/lists/*

# Install Rust
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

# Add Cargo to PATH
ENV PATH="/root/.cargo/bin:${PATH}"

# Create a working directory
WORKDIR /usr/src/get_current_time

# Copy your source code
COPY . .

# Build the Rust application
RUN cargo build --release

# Stage 2: Runtime Environment
FROM debian:buster-slim

# Install runtime dependencies, if any
RUN apt-get update \
    && apt-get install -y ca-certificates \
    && rm -rf /var/lib/apt/lists/*

# Copy the binary from the builder stage
COPY --from=builder /usr/src/get_current_time/target/release/actix_web_app /usr/local/bin/get_current_time

# Command to run the application
CMD ["get_current_time"]

