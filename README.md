# Individual Project2: Continuous Delivery of Rust Microservice
> Xingyu, Zhang (NetID: xz365)

## Setting Up The Rust Project
#### Create a New Project:
```bash
cargo new project_name
cd project_name
```
#### Add Dependencies (in [```Cargo.toml```](./Cargo.toml)):
```toml
actix-web = "4"
tokio = { version = "1", features = ["full"] }
chrono = "0.4.34"
```
#### Create a Simple Web Server (in [```./src/main.rs```](./src/main.rs)): 

This code sets up a simple Actix web server in Rust. It defines two asynchronous functions, ```get_time()``` and ```time_zone()```, which return the current time in UTC and the local time zone respectively. The main function sets up the Actix web server, defining routes for handling requests to "/" and "/local". When a request is made to these routes, it will call the corresponding asynchronous functions to handle the request and respond with the current time. Finally, the server is bound to listen on port 8080 on all available network interfaces.

```rust
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use chrono::prelude::*;

async fn get_time() -> impl Responder {
    let current_time = Utc::now().to_string();
    HttpResponse::Ok().body(format!("Hello visitor!\nCurrent time is {}", current_time))
}

async fn time_zone() -> impl Responder {
    let current_time = Utc::now();
    let local_time = current_time.with_timezone(&Local);
    HttpResponse::Ok().body(format!("In your time zone, it is {} now.", local_time))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(get_time))
            .route("/local", web::get().to(time_zone))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
```
#### Verify The Rust Application Locally: 
Ensure the Rust application compiles and runs correctly on the local machine.
```bash
cargo build --release
```
```bash
cargo run
```

- Results:
![](./assets/utc.png)
![](./assets/local.png)

## Setting Up Docker
1. Download [Docker](https://docs.docker.com/get-docker/).
2. Install Docker Following the Official Doc: [Mac](https://docs.docker.com/desktop/install/mac-install/) | [Windows](https://docs.docker.com/desktop/install/windows-install/) | [Linux](https://docs.docker.com/desktop/install/linux-install/) .

## Containerize The Rust Actix Web Service
[```Dockerfile```](./Dockerfile)

#### Build the Docker Image
```bash
docker build -t get_current_time .
```
or __No Cache Build__: Try building the Docker image with the ```--no-cache``` option to ensure it doesn't use the cached layers:
```bash
docker build --no-cache -t get_current_time .
```

#### Run the Container Locally
```bash
docker run -p 8080:8080 get_current_time
```
This command maps port 8080 from the container to port 8080 on local host, allowing accessing to the web service from the local machine.

- Docker Desktop State:
![docker](./assets/docker_image.png)

## CI/CD pipeline
Set up the configure file [```.gitlab-ci.yml```](./.gitlab-ci.yml)
```yaml
stages:
  - build
  - test

variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_PROJECT_PATH:$CI_COMMIT_REF_SLUG

build:
  stage: build
  image: docker:stable
  services:
    - docker:dind
  script:
    - docker build -t get_current_time .
    - docker run -d -p 8080:8080 get_current_time
    - docker ps -a

test:
  stage: test
  image: rust:latest
  script:
  - cargo test --verbose
```

- The pipeline run sucessfully:
![pipeline](./assets/pipeline.png)

- Demo Video:
![demo](./assets/individualProject2.mkv)
