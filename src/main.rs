use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use chrono::prelude::*;

async fn get_time() -> impl Responder {
    let current_time = Utc::now().to_string();
    HttpResponse::Ok().body(format!("Hello visitor!\nCurrent time is {}", current_time))
}

async fn time_zone() -> impl Responder {
    let current_time = Utc::now();
    let local_time = current_time.with_timezone(&Local);
    HttpResponse::Ok().body(format!("In your time zone, it is {} now.", local_time))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(get_time))
            .route("/local", web::get().to(time_zone))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
